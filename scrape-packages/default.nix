# To pin to a specific version of nixpkgs, you can substitute <nixpkgs> with:
# `(builtins.fetchTarball "https://github.com/NixOS/nixpkgs/archive/<nixpkgs_commit_hash>.tar.gz")`
{ compiler ? "ghc822"
, pkgs     ? import <nixpkgs> {} }:
  pkgs.haskell.packages.${compiler}.developPackage {
    root = ./.;
    overrides = self: super: with pkgs.haskell.lib; {
      # Don't run a package's test suite
      # foo = dontCheck super.foo;
      #
      # Don't enforce package's version constraints
      # bar = doJailbreak super.bar;
      #
      # To discover more functions that can be used to modify haskell
      # packages, run "nix-repl", type "pkgs.haskell.lib.", then hit
      # <TAB> to get a tab-completed list of functions.
    };
    source-overrides = {
      # Use a specific hackage version
      # bytestring = "0.10.8.1";
      #
      # Use a particular commit from github
      # parsec = pkgs.fetchFromGitHub
      #   { owner = "hvr";
      #     repo = "parsec";
      #     rev = "c22d391c046ef075a6c771d05c612505ec2cd0c3";
      #     sha256 = "0phar79fky4yzv4hq28py18i4iw779gp5n327xx76mrj7yj87id3";
      #   };
    };
  }
