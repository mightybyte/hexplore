{-# LANGUAGE OverloadedStrings #-}

module Main where

------------------------------------------------------------------------------
import           Control.Concurrent
import           Control.Error
import           Control.Lens (to, only, (^.), (^?), ix)
import           Control.Monad
import qualified Data.Aeson as A
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import           Data.List
import           Data.List.Split
import           Data.Map (Map)
import qualified Data.Map as M
import           Data.String.Conv
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Text.Encoding.Error (lenientDecode)
import           Data.Text.Lazy.Encoding (decodeUtf8With)
import           Data.Time
import qualified Data.Vector as V
import           Distribution.PackageDescription.Parse
import           Distribution.PackageDescription
import           Distribution.Types.PackageId
import           Distribution.Types.PackageName
import           Distribution.Verbosity
import           Distribution.Version
import           Network.Http.Client
import           System.Directory
import           System.Environment
import           System.FilePath
import           System.Process
import           Text.Printf
import           Text.Taggy (Node)
import           Text.Taggy.Lens --(html, elements, children, contents,allNamed)
--import qualified Text.Taggy.Lens as Taggy

------------------------------------------------------------------------------
import           PackageData
------------------------------------------------------------------------------

main :: IO ()
main = do
  args <- getArgs
  let outputLoc = case args of
                    [s] -> s
                    _ -> error "Must specify the output directory as an argument"
  t <- getCurrentTime
  putStrLn $ "Starting scrape at " ++ show t
  putStrLn $ "Writing output to " ++ outputLoc
  let dir = "/tmp/hackage-index-" ++ (formatTime defaultTimeLocale "%Y%m%d-%H%M" t)
      index = dir </> "01-index.tar.gz"
  createDirectory dir
  setCurrentDirectory dir
  putStrLn "Downloading hackage index"
  bs <- get "http://hackage.haskell.org/01-index.tar.gz" concatHandler
  putStrLn "Saving to disk"
  B.writeFile index bs
  threadDelay 10000000
  putStrLn "Unzipping index"
  system $ "tar xvfz " ++ index ++ " > /dev/null"
  putStrLn "Constructing pdata"
  go dir (outputLoc </> "pdata.json")
  --removeDirectoryRecursive dir

go :: FilePath -> FilePath -> IO ()
go root outfile = do
  files <- getMostRecentCabalFiles root
  hdata <- getHackageData
  pds <- mapM (getCabalStuff hdata) files
  L.writeFile outfile $ A.encode $ A.Array $ V.fromList $ map A.toJSON pds

getMostRecentCabalFiles :: FilePath -> IO [FilePath]
getMostRecentCabalFiles root = do
  pkgs <- listDirectory root
  mapM (getMostRecent root) =<< filterM (doesDirectoryExist . (root </>)) pkgs

getMostRecent :: FilePath -> FilePath -> IO FilePath
getMostRecent root pkg = do
  let pkgRoot = root </> pkg
  rawVers <- filterM (doesDirectoryExist . (pkgRoot </>)) =<< listDirectory pkgRoot
  let myRead s = case readMay s of
                   Nothing -> error $ printf "Error parsing %s from package %s\n" s pkg
                   Just a -> a
  let parseVersion = mkVersion . map myRead . splitOn "."
  let vers = map parseVersion rawVers
  let mostRecent = head $ reverse $ sort vers
  return $ pkgRoot </> showVersion mostRecent </> pkg <.> "cabal"

getCabalStuff :: Map Text HackageData -> FilePath -> IO PackageData
getCabalStuff hdMap f = do
    p <- readGenericPackageDescription silent f
    let d = packageDescription p
        n = T.pack $ unPackageName $ pkgName $ package d
        hd = M.lookup n hdMap
    return $ PackageData n (T.pack $ synopsis d) (T.pack $ description d)
      (T.pack $ category d) (T.pack $ author d) (T.pack $ maintainer d)
      (hdDownloads <$> hd) (hdLastUploaded <$> hd)


data HackageData = HackageData
    { hdDownloads :: Int
    , hdLastUploaded :: Day
    }
  deriving (Eq,Ord,Show,Read)

prettyHackageData :: (Text, HackageData) -> String
prettyHackageData (n, HackageData d u) = unwords [T.unpack n, show d, show u]

getHackageData :: IO (Map Text HackageData)
getHackageData = do
  bs <- get "http://hackage.haskell.org/packages/browse" concatHandler
  let trs = (toS bs) ^. to (decodeUtf8With lenientDecode) . html .
        allNamed (only "tbody") . element . children
  return $ M.fromList $ catMaybes $ map extractData trs


extractData :: Node -> Maybe (Text, HackageData)
extractData tr = do
  row <- tr ^? element . children
  nm <- row ^? ix 0 . element . children . ix 0 . element . contents
  downloads <- row ^? ix 1 . element . contents
  lastUpload <- row ^? ix 5 . element . contents
  day <- parseTimeM True defaultTimeLocale "%F" $ T.unpack lastUpload
  return (nm, HackageData (read $ T.unpack downloads) day)
