{ rpRef ? "ea3c9a1536a987916502701fb6d319a880fdec96" }:

let rp = builtins.fetchTarball "https://github.com/reflex-frp/reflex-platform/archive/${rpRef}.tar.gz";

in

(import rp {}).project ({ pkgs, ... }:
with pkgs.haskell.lib;
{
  name = "hexplore";
  overrides = self: super: {

    # Cabal = dontCheck (self.callHackage "Cabal" "2.0.0.2" {});
    heist = dontCheck (self.callCabal2nix "heist" (pkgs.fetchFromGitHub {
      owner = "snapframework";
      repo = "heist";
      rev = "3ccbec548830abce7ed7eba42c1c294b02b6cd52";
      sha256 = "14sd4d4an7fj8yb4mr8cdallsv69x5jb1hd330sg10ahi1ryzspr";
    }) {});
    map-syntax = self.callCabal2nix "map-syntax" (pkgs.fetchFromGitHub {
      owner = "mightybyte";
      repo = "map-syntax";
      rev = "acebcf0a83ee639e1a0c49850b9c85821d53f621";
      sha256 = "076knpvls1489gish9z30lhb21vqx44k366vc2i3kdql815v1vqv";
    }) {};
    reflex-dom-contrib = doJailbreak (dontCheck (self.callCabal2nix "reflex-dom-contrib" (pkgs.fetchFromGitHub {
      owner = "reflex-frp";
      repo = "reflex-dom-contrib";
      rev = "b47f90c810c838009bf69e1f8dacdcd10fe8ffe3";
      sha256 = "0yvjnr9xfm0bg7b6q7ssdci43ca2ap3wvjhshv61dnpvh60ldsk9";
    }) {}));
    snap = doJailbreak (dontCheck (self.callCabal2nix "snap" (pkgs.fetchFromGitHub {
      owner = "snapframework";
      repo = "snap";
      rev = "71a6072ceb136602aa1c45bc7bbfed7006945030";
      sha256 = "1n5cpsv2asp9bwzx6bncj0r0cb0nb5v9rrlfz5hjn9padl45ndzx";
    }) {}));
  };
  packages = {
    backend = ./backend;
    common = ./common;
    frontend = ./frontend;
  };

  shells = {
    ghc = ["common" "backend" "frontend"];
    ghcjs = ["common" "frontend"];
  };
})
