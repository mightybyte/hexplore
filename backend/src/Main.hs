{-# LANGUAGE CPP             #-}
{-# LANGUAGE TemplateHaskell #-}

module Main where

------------------------------------------------------------------------------
import           Control.Exception (SomeException, try)
import qualified Data.Text as T
import           Snap.Http.Server
import           Snap.Loader.Static
import           Snap.Snaplet
import           Snap.Snaplet.Config
import           Snap.Core
import           System.IO
import           Site
------------------------------------------------------------------------------


------------------------------------------------------------------------------
main :: IO ()
main = do
    conf <- commandLineAppConfig defaultConfig
    (msgs, site, cleanup) <- runSnaplet
        (appEnvironment =<< getOther conf) app
    hPutStrLn stderr $ T.unpack msgs
    _ <- try $ httpServe conf site :: IO (Either SomeException ())
    cleanup
